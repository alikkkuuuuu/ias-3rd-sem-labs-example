﻿using System;

namespace Classes
{
    class Program
    {
        enum Material
        {
            Tissue,     // Ткань
            EcoLeather, // Эко-кожа
            Gunny,      // Рогожка
        }

        enum SofaType
        {
            Straight,   // Прямой
            Corner,     // Угловой
            Children,   // Детский
        }

        class Sofa
        {
            int height;         // Высота (см.)
            int width;          // Ширина (см.)
            Material material;  // Обвивка
            bool laundryBox;    // Ящик для белья
            SofaType type;      // Тип дивана

            public static int count = 0;    // Счётчик

            public void PrintFields()
            {
                Console.WriteLine("-- Диван --\n"
                            + $"Высота: {height}\n"
                            + $"Ширина: {width}\n"
                            + $"Обвивка: {material}\n"
                            + $"Тип: {type}\n");
            }

            public void InitFields(int height, int width,
                Material material, bool laundryBox, SofaType type)
            {
                this.height     = height;
                this.width      = width;
                this.material   = material;
                this.laundryBox = laundryBox;
                this.type       = type;
            }

            public Sofa()
            {
                count++;
            }
        }

        static void Main()
        {
            Sofa cornerTallSofa       = new Sofa();
            Sofa childrenWideSofa     = new Sofa();
            Sofa ecoLeatherCornerSofa = new Sofa();

            cornerTallSofa.InitFields(100, 150, Material.Tissue, false, SofaType.Corner);
            childrenWideSofa.InitFields(80, 150, Material.Tissue, false, SofaType.Children);
            ecoLeatherCornerSofa.InitFields(90, 190, Material.EcoLeather, true, SofaType.Corner);

            foreach (Sofa s in new Sofa[] {cornerTallSofa, childrenWideSofa, ecoLeatherCornerSofa})
            {
                s.PrintFields();
            }

            Console.WriteLine("Количество созданных объектов: " + Sofa.count);
        }
    }
}
