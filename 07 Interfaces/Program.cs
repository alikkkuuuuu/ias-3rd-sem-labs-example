﻿using System;

namespace Interfaces
{
    struct Pair
    {
        public int Horizontal { get; set; }
        public int Vertical { get; set; }

        public Pair(int horiz, int vert)
        {
            if (horiz > 0 && horiz < 9)
                Horizontal = horiz;
            else
                throw new ArgumentOutOfRangeException();

            Vertical = vert;
        }

        public Pair(char horiz, int vert)
        {
            if (horiz > 'A' && horiz < 'H')
                Horizontal = horiz - 'A' + 1;
            else if (horiz > 'a' && horiz < 'h')
                Horizontal = horiz - 'a' + 1;
            else throw new ArgumentOutOfRangeException();

            Vertical = vert;
        }

        public override string ToString()
        {
            return $"{Horizontal} - {Vertical}";
        }
    }

    static class ChessBoard
    {
        public static ChessFigure[,] Figures { get; private set; } = new ChessFigure[8, 8];

        public static void PlaceFigure(ChessFigure chessFigure)
        {
            if (Figures[chessFigure.Position.Horizontal - 1, chessFigure.Position.Vertical - 1] != null)
                throw new ArgumentException("Место занято");
            Figures[chessFigure.Position.Horizontal - 1, chessFigure.Position.Vertical - 1] = chessFigure;
        }

        public static void PrintBoard()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (Figures[i, j] == null)
                        Console.Write("Null\t");
                    else
                        Console.Write(Figures[i, j].ToString().Remove(
                            0, Figures[i, j].ToString().IndexOf(".") + 1) + "\t");
                }
                Console.WriteLine("\n");
            }
        }
    }

    interface ICorrect
    {
        Pair[] PossibleMoves();
        bool IsCorrectMove(Pair pos);
    }

    abstract class ChessFigure : ICorrect
    {
        public Pair Position { get; private set; }
        public bool IsWhite { get; }

        public abstract Pair[] PossibleMoves();
        public virtual bool IsCorrectMove(Pair pos)
        {
            return PossibleMoves().Contains(pos);
        }

        public ChessFigure(Pair pos, bool isWhite)
        {
            Position = pos;
            IsWhite = isWhite;
            ChessBoard.PlaceFigure(this);
        }
    }

    class Horse : ChessFigure
    {
        public override Pair[] PossibleMoves()
        {
            List<Pair> pairs = new List<Pair>(8);

            // Пары x, y возможных ходов коня
            int[] possibleX = { 2, 1, -1, -2, -2, -1, 1, 2 };
            int[] possibleY = { 1, 2, 2, 1, -1, -2, -2, -1 };

            for (int i = 0; i < 8; i++)
            {
                // Позиция коня после передвижения
                int xPos = Position.Horizontal + possibleX[i] - 1;
                int yPos = Position.Vertical + possibleY[i] - 1;

                if (xPos >= 0 && yPos >= 0 &&
                    xPos < 8 && yPos < 8 &&
                    ChessBoard.Figures[xPos, yPos] == null)
                    pairs.Add(new Pair(xPos + 1, yPos + 1));
            }

            return pairs.ToArray();
        }

        public Horse(Pair pos, bool isWhite) : base(pos, isWhite) { }
    }

    class Rook : ChessFigure
    {
        private void bruteCheck(int start, bool horiz, in List<Pair> pairs)
        {
            int top = start;
            int bottom = start - 2;

            while (top < 8 || bottom >= 0)
            {
                if (horiz)
                {
                    if (top < 8 && ChessBoard.Figures[top, Position.Vertical - 1] == null)
                        pairs.Add(new Pair(top + 1, Position.Vertical));
                    if (bottom >= 0 && ChessBoard.Figures[bottom, Position.Vertical - 1] == null)
                        pairs.Add(new Pair(bottom + 1, Position.Vertical));
                }
                else
                {
                    if (top < 8 && ChessBoard.Figures[Position.Horizontal - 1, top] == null)
                        pairs.Add(new Pair(Position.Horizontal, top + 1));
                    if (bottom >= 0 && ChessBoard.Figures[Position.Horizontal - 1, bottom] == null)
                        pairs.Add(new Pair(Position.Horizontal, bottom + 1));
                }

                top++;
                bottom--;
            }
        }

        public override Pair[] PossibleMoves()
        {
            List<Pair> pairs = new List<Pair>(16);

            bruteCheck(Position.Horizontal, true, pairs);
            bruteCheck(Position.Vertical, false, pairs);

            return pairs.ToArray();
        }

        public Rook(Pair pos, bool isWhite) : base(pos, isWhite) { }
    }

    class Queen : ChessFigure
    {
        private void bruteCheckRook(int start, bool horiz, in List<Pair> pairs)
        {
            int top = start;
            int bottom = start - 2;

            while (top < 8 || bottom >= 0)
            {
                if (horiz)
                {
                    if (top < 8 && ChessBoard.Figures[top, Position.Vertical - 1] == null)
                        pairs.Add(new Pair(top + 1, Position.Vertical));
                    if (bottom >= 0 && ChessBoard.Figures[bottom, Position.Vertical - 1] == null)
                        pairs.Add(new Pair(bottom + 1, Position.Vertical));
                }
                else
                {
                    if (top < 8 && ChessBoard.Figures[Position.Horizontal - 1, top] == null)
                        pairs.Add(new Pair(Position.Horizontal, top + 1));
                    if (bottom >= 0 && ChessBoard.Figures[Position.Horizontal - 1, bottom] == null)
                        pairs.Add(new Pair(Position.Horizontal, bottom + 1));
                }

                top++;
                bottom--;
            }
        }

        private void bruteCheckBishop(int horiz, int vert, in List<Pair> pairs)
        {
            int topRow = horiz;
            int botRow = horiz - 2;

            int topCol = vert;
            int botCol = vert - 2;

            while (topRow < 8 || topCol < 8 || botRow >= 0 || botCol >= 0)
            {
                if (topRow < 8 && topCol < 8 && ChessBoard.Figures[topRow, topCol] == null)
                    pairs.Add(new Pair(topRow + 1, topCol + 1));
                    
                if (topRow < 8 && botCol >= 0 && ChessBoard.Figures[topRow, botCol] == null)
                    pairs.Add(new Pair(topRow + 1, botCol + 1));

                if (botRow >= 0 && topCol < 8 && ChessBoard.Figures[botRow, topCol] == null)
                    pairs.Add(new Pair(botRow + 1, topCol + 1));

                if (botRow >= 0 && botCol >= 0 && ChessBoard.Figures[botRow, botCol] == null)
                    pairs.Add(new Pair(botRow + 1, botCol + 1));

                topRow++;
                topCol++;
                botRow--;
                botCol--;
            }
        }

        public override Pair[] PossibleMoves()
        {
            List<Pair> pairs = new List<Pair>(32);

            bruteCheckRook(Position.Horizontal, true, pairs);
            bruteCheckRook(Position.Vertical, false, pairs);

            bruteCheckBishop(Position.Horizontal, Position.Vertical, pairs);

            return pairs.ToArray();
        }

        public Queen(Pair pos, bool isWhite) : base(pos, isWhite) { }
    }

    class Pawn : ChessFigure
    {
        public override Pair[] PossibleMoves()
        {
            List<Pair> pairs = new List<Pair>(2);

            if (IsWhite)
                for (int i = Position.Vertical; i < Position.Vertical + 2 && i < 8; i++)
                {
                    if (ChessBoard.Figures[Position.Horizontal - 1, i] != null)
                        break;
                    else
                        pairs.Add(new Pair(Position.Horizontal, i + 1));
                }
            else
                for (int i = Position.Vertical - 2; i > Position.Vertical - 4 && i >= 0; i--)
                {
                    if (ChessBoard.Figures[Position.Horizontal - 1, i] != null)
                        break;
                    else
                        pairs.Add(new Pair(Position.Horizontal, i + 1));
                }

            return pairs.ToArray();
        }

        public Pawn(Pair pos, bool isWhite) : base(pos, isWhite) { }
    }

    class Program
    {
        static void Main()
        {
            var figure = new Queen(new Pair(8, 8), false);

            foreach (var p in figure.PossibleMoves())
                Console.Write(p.ToString() + ", ");

            Console.WriteLine();
        }
    }
}