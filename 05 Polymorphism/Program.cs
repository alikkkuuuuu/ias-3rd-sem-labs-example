﻿using System;

namespace Polymorphism
{
    abstract class Figure
    {
        public abstract double Volume { get; }
    }

    class Cube : Figure
    {
        double sideLength;

        public override double Volume
        {
            get
            {
                return Math.Pow(sideLength, 3);
            }
        }

        public Cube(double sideLength)
        {
            this.sideLength = sideLength;
        }
    }

    class Pyramid : Figure
    {
        double height;
        double length;
        double breadth;

        public override double Volume
        {
            get
            {
                return (length * breadth * height) / 3d;
            }
        }

        public Pyramid(double height, double length, double breadth)
        {
            this.height = height;
            this.length = length;
            this.breadth = breadth;
        }
    }

    class Sphere : Figure
    {
        double radius;

        public override double Volume
        {
            get
            {
                return (4d / 3d) * Math.PI * Math.Pow(radius, 3);
            }
        }

        public Sphere(double radius)
        {
            this.radius = radius;
        }
    }

    class Program
    {
        static void Main()
        {
            Figure[] figures = { new Cube(5), new Pyramid(3, 4, 5), new Sphere(3) };

            foreach (var f in figures)
                Console.WriteLine($"{f.GetType()} Volume \t- {f.Volume.ToString("F")}");
        }
    }
}