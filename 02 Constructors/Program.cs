﻿using System;

namespace Constructors
{
    class Program
    {
        enum Material
        {
            Tissue,     // Ткань
            EcoLeather, // Эко-кожа
            Gunny,      // Рогожка
        }

        enum SofaType
        {
            Straight,   // Прямой
            Corner,     // Угловой
            Children,   // Детский
        }

        class Sofa
        {
            int height;         // Высота (см.)
            int width;          // Ширина (см.)
            Material material;  // Обвивка
            bool laundryBox;    // Ящик для белья
            SofaType type;      // Тип дивана

            public static int randValue;    // Случайное значение

            public Sofa()
            {
                height = 100;
                width = 150;
                material = Material.Tissue;
                laundryBox = false;
                type = SofaType.Straight;
            }

            public Sofa(int height, int width, 
                Material material, bool laundryBox, SofaType type)
            {
                this.height     = height;
                this.width      = width;
                this.material   = material;
                this.laundryBox = laundryBox;
                this.type       = type; 
            }

            static Sofa()
            {
                Random rand = new Random();
                randValue = rand.Next();
            }
        }

        static void Main()
        {
            Console.WriteLine("Случайное число: " + Sofa.randValue);

            Sofa cornerTallSofa   = new Sofa(100, 150, Material.Tissue, false, SofaType.Corner);
            Sofa childrenWideSofa = new Sofa(80, 150, Material.Tissue, false, SofaType.Children);
            Sofa genericSofa      = new Sofa();

            Console.WriteLine("Случайное число: " + Sofa.randValue);
        }
    }
}
