﻿using System;

namespace Inheritance
{
    abstract class MusicalInstrument
    {
        // Минимальная и максимальная частота инструмента
        public abstract int LowestFrequency { get; }
        public abstract int HighestFrequency { get; }

        // Максимальный обертон
        public abstract int MaximumOvertone { get; }

        // Строковое представление звука инструмента
        public abstract string Sound { get; }

        // Метод игры на инструменте
        public virtual void Play()
        {
            Random r = new Random();
            Console.WriteLine($"{Sound} goes brr... on {r.Next(LowestFrequency, HighestFrequency)}Hz");
        }
    }

    class Violin : MusicalInstrument
    {
        public override int LowestFrequency { get; }  = 196;
        public override int HighestFrequency { get; } = 2100;
        public override int MaximumOvertone { get; }  = 10000;
        public override string Sound { get; } = "Violin";

        // Представляет размер скрипки в виде 1 / n
        public byte SizeFraction { get; } = 1;

        public Violin(byte size)
        {
            SizeFraction = size;
        }

        public Violin() {}

        public override void Play()
        {
            if (SizeFraction > 1 && SizeFraction < 32)
                Console.Write("Small ");
            else if (SizeFraction >= 32)
                Console.Write("Very small ");

            base.Play();
        }
    }

    class Piano : MusicalInstrument
    {
        public override int LowestFrequency { get; }  = 27;
        public override int HighestFrequency { get; } = 4186;
        public override int MaximumOvertone { get; }  = 13000;
        public override string Sound { get; } = "Piano";

        // Свойство, определяющее макс. кол-во нот на пианино
        public int NotesCount { get; } = 88;

        // Метод, позволяющий получить частоту на определённой ноте пианино
        public int GetFrequencyOnNote(int n)
        {
            if (n < 1 && n > NotesCount)
            {
                return -1;
            }
            else
            {
                // Формула для расчёта частоты на определённой ноте пианино
                int freq = (int)Math.Pow(Math.Pow(2, 1f/12f), n - (NotesCount / 2) - 5) * 440;
                return freq;
            }
        }

        public void PlayOnNote(int n)
        {
            Console.WriteLine($"{Sound} goes brr... on note {n} on {GetFrequencyOnNote(n)}Hz");
        }

        // Конструктор, позволяющий задать макс. кол-во нот
        public Piano(int notes)
        {
            if (notes < 1) return;

            NotesCount = notes;
            HighestFrequency = GetFrequencyOnNote(notes);
            MaximumOvertone = (int)(Math.Log(HighestFrequency, 2) * 1000);
        }

        public Piano() {}
    }

    enum AcousticGuitarType
    {
        Dreadnought,
        Jumbo,
        Auditorium,
        Classical,
        Parlor,
        Travel,
    }

    class AcousticGuitar : MusicalInstrument
    {
        public override int LowestFrequency { get; }  = 82;
        public override int HighestFrequency { get; } = 1175;
        public override int MaximumOvertone { get; }  = 12000;
        public override string Sound { get; } = "Acoustic Guitar";

        // Представляет тип акустической гитары
        public AcousticGuitarType Type { get; } = AcousticGuitarType.Classical;

        public AcousticGuitar(AcousticGuitarType type)
        {
            Type = type;
        }

        public AcousticGuitar() {}

        public override void Play()
        {
            Console.Write(Type.ToString() + " ");
            base.Play();
        }
    }

    class Program
    {
        static void Main()
        {
            MusicalInstrument genericPiano = new Piano();
            MusicalInstrument smallViolin = new Violin(64);
            MusicalInstrument dreadNoughtGuitar = new AcousticGuitar(AcousticGuitarType.Dreadnought);

            smallViolin.Play();
            dreadNoughtGuitar.Play();
            genericPiano.Play();
        }
    }
}
