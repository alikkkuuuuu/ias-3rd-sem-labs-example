﻿using System;
using System.Runtime.Serialization;

namespace Exceptions
{
    class NoVowelsInWordException : Exception
    {
        public NoVowelsInWordException()
            : base()
        { }

        public NoVowelsInWordException(string msg)
            : base(msg)
        { }

        public NoVowelsInWordException(string msg, Exception e)
            : base(msg, e)
        { }

        protected NoVowelsInWordException(SerializationInfo info,
                                       StreamingContext context)
            : base(info, context)
        { }
    }

    class Program
    {
        static void Main()
        {
            char[] rusVowels = { 'а', 'о', 'у', 'э', 'и', 'ы', 'я', 'ё', 'ю', 'е' };

            string word = "ввп";

            bool contains = false;
            foreach (var c in word)
            {
                if (rusVowels.Contains(c))
                    contains = true;
            }

            if (!contains)
                throw new NoVowelsInWordException("Нет гласных");
        }
    }
}