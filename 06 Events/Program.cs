﻿using System;

namespace Events
{
    enum Manufacturer
    {
        Apple,
        Xiaomi,
        Meizu,
    }

    class Phone
    {
        int chargeLevel;

        public Manufacturer Manufacturer { get; }

        public DateTime DateOfCreation { get; }

        public int DischarginRate { get; private set; }

        public string LastNotification { get; private set; }

        public bool AtLowCharge
        {
            get
            {
                return ChargeLevel <= 20;
            } 
        }

        public int ChargeLevel 
        { 
            get { return chargeLevel; } 
            private set
            {
                chargeLevel = value;

                if (chargeLevel > 100)
                    chargeLevel = 100;
                else if (chargeLevel < 0)
                    chargeLevel = 0;

                if (AtLowCharge)
                    OnLowCharge?.Invoke();
            }
        }

        public delegate void PhoneHandler();
        public event PhoneHandler OnLowCharge;

        public void Use()
        {
            if (ChargeLevel == 0)
            {
                OnLowCharge?.Invoke();
                return;
            }

            Thread.Sleep(100);
            ChargeLevel -= DischarginRate;
        }
        
        public void Charge()
        {
            ChargeLevel += 5;
            Thread.Sleep(2000);
        }

        public void Notify(string notification)
        {
            LastNotification = notification;
        }

        public Phone(Manufacturer manufacturer)
        {
            Manufacturer = manufacturer;
            DateOfCreation = DateTime.Now;
            ChargeLevel = 100;
            LastNotification = "";

            DischarginRate = Manufacturer == Manufacturer.Apple ? 5 : 2;
        }
    }

    class PhoneOwner
    {
        Phone phone;

        public string Name { get; }
        public string SecondName { get; }
        public Manufacturer PhoneManufacturer => phone.Manufacturer;
        
        public void ReadNews()    
        {
            phone.Use();
            Thread.Sleep(400);

            Console.WriteLine("Телефон разряжается... " + phone.ChargeLevel + "%");
        }

        public void ChargePhone() 
        {
            Console.WriteLine("Вызвалось событие! Зарядка телефона: " + phone.ChargeLevel);

            while (phone.ChargeLevel < 30)
            {
                phone.Charge();
                Console.WriteLine("Телефон заряжается... " + phone.ChargeLevel + "%");
            }
        }

        public void BuyNewPhone(Phone p) => phone = p;

        public PhoneOwner(string name, string secondName, Phone p)
        {
            Name = name;
            SecondName = secondName;
            phone = p;

            phone.OnLowCharge += ChargePhone;
        }
    }

    class Program
    {
        static void Main()
        {
            Phone genericIPhone = new Phone(Manufacturer.Apple);
            PhoneOwner appleUser = new PhoneOwner("Valeriy", "Zhdanov", genericIPhone);

            for (int i = 0; i < 20; i++)
                appleUser.ReadNews();
        }
    }
}