﻿using System;

namespace Incapsulation
{
    enum PhoneCallType
    {
        Incoming,
        Outcoming,
    }

    class PhoneCall
    {
        public DateTime CallTime { get; set; }  // Время начала звонка
        public TimeSpan Duration { get; set; }  // Продолжительность
        public PhoneCallType Type { get; set; } // Тип звонка
        public bool IsBilled { get; set; }      // Взаимается ли плата
        public string Number { get; set; }      // Номер, с которого звонят

        // Индексатор, подаётся индекс и возвращается
        // символ на данном индексе в строке номера
        public char this[int n]
        {
            get => Number[n];
        }

        // Конструктор принимающий все значения
        public PhoneCall(TimeSpan length, PhoneCallType type,
            bool isBilled, string number)
        {
            CallTime = DateTime.Now;
            Duration = length;
            Type     = type;
            IsBilled = isBilled;
            Number   = number;
        }

        // Перегрузка конструктора, где вводятся минуты
        public PhoneCall(int minutes, PhoneCallType type,
            bool isBilled, string number)
            : this (TimeSpan.FromMinutes(minutes), type, isBilled, number)
        {
        }

        // Вывод всех свойств на экран
        public void PrintFields()
        {
            Console.WriteLine($"{nameof(CallTime)} \t {CallTime}");
            Console.WriteLine($"{nameof(Duration)} \t {Duration}");
            Console.WriteLine($"{nameof(Type)} \t {Type}");
            Console.WriteLine($"{nameof(IsBilled)} \t {IsBilled}");
            Console.WriteLine($"{nameof(Number)} \t {Number}");
        }
    }

    class Programm
    {
        static void Main(string[] args)
        {
            int min;
            string num;
            bool billed;
            PhoneCallType type;

            min = int.Parse(args[0]);
            billed = bool.Parse(args[1]);

            switch (args[2])
            {
                case "in":
                    type = PhoneCallType.Incoming;
                    break;
                case "out":
                    type = PhoneCallType.Outcoming;
                    break;
                default:
                    type = PhoneCallType.Incoming;
                    break;
            }

            num = args[3];

            PhoneCall pc = new PhoneCall(min, type, billed, num);
            pc.PrintFields();

            Console.WriteLine(pc[4]);
        }
    }
}
